import json
from flask import Flask, render_template
from RandomDealData import generate_mutable_deals

app = Flask(__name__)

@app.route("/generateData")
def hello():
    answer = generate_mutable_deals(5)
    print(answer)
    return render_template('deals.html', data=answer)

def bootapp():
    app.run(port=7000, threaded=True, host=('0.0.0.0'))

if __name__ == '__main__':
     bootapp()