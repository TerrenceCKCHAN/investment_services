import unittest
from RandomDealData import *

class TestRandomDealData(unittest.TestCase):
    def testCreateInstrumentList(self):
        rdt = RandomDealData()
        resList = rdt.createInstrumentList()
        assert isinstance(resList, list)

if __name__ == '__main__':
    unittest.main()

