from flask import Flask, render_template, jsonify, request
from flask_cors import CORS
import json


app = Flask(__name__)
CORS(app, supports_credentials=True)
 
@app.route("/", methods=['POST'])
def hello():
    return jsonify(name="Hello World")

    # return render_template('capitals.html', data=capitals)
 
@app.route("/submitForm", methods=['POST'])
def submitForm():
    req = request.json
    print(req)
    return jsonify(name='username')

def bootapp():
    app.run(port=8080, threaded=True, host=('localhost'))

if __name__ == '__main__':
     bootapp()