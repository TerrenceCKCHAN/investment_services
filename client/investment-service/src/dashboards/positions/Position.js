import React from 'react';
import { Grid } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

class Position extends React.Component {

    render() {
        return (
                <Grid.Row className="companylist__item">
                  <Grid.Column
                    as={Link}
                    width={7}
                    className="companylist__item__info"
                    to={"XX"}
                  >
                    <span className="name">{"Positionname"}</span>
                    <small className="ticker">{"Positioninfo"}</small>
                  </Grid.Column>
                  <Grid.Column
                    width={6}
                    only="computer"
                    style={{ justifyContent: 'flex-end' }}
                    className="companylist__item__curve"
                  >XXX
                    {/* <PositionChart
                      stock={stock}
                      deltaPrice={delta}
                      height={35}
                      width={175}
                    /> */}
                  </Grid.Column>
                  <Grid.Column width={3} className="companylist__item__delta">
                    <p>XXX</p>
                    {/* <PriceChangeLabel
                      delta={delta}
                      deltaPercent={deltaPercent}
                      polarity={company.mpa_polarity}
                    /> */}
                  </Grid.Column>
                </Grid.Row>
            
        );
    }
}

export default Position;