import React, { Component } from 'react'
import { Menu } from 'semantic-ui-react'
import { Link } from 'react-router-dom'

export default class DashMenu extends Component {
    state = {}

    handleItemClick = (e, { name }) => this.setState({ activeItem: name })

    render() {
        const { activeItem } = this.state

        return (
            <Menu>
                <Menu.Item
                    name='My Finances'
                    active={activeItem === 'My Finances'}
                    onClick={this.handleItemClick}
                    as={ Link }
                    to='finances'
                >
                    My Finances
                </Menu.Item>

                <Menu.Item name='Instrument Data' active={activeItem === 'Instrument Data'} onClick={this.handleItemClick}>
                    Instrument Data
                </Menu.Item>
            </Menu>
        )
    }
}