import axios from 'axios';

const api = axios.create({
    baseURL: 'http://localhost:8080/',
    withCredentials: true,
});

api.defaults.headers.post['Content-Type'] = 'application/json';

export async function APIsubmitForm(formData) {
    return api.request({
        url: "/submitForm",
        method: 'post',
        data: JSON.stringify(formData)
    })
}