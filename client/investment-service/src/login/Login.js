import React from 'react'
import { Button, Form, Grid, Header, Segment } from 'semantic-ui-react'
import { APIsubmitForm } from '../api.js'

class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        };

        this.handleChangeUser = this.handleChangeUser.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeUser(event) {
        this.setState({ username: event.target.value });
    }
    handleChangePassword(event) {
        this.setState({ password: event.target.value });
    }

    handleSubmit() {
        const json = {
            username: this.state.username,
            password: this.state.password
        }
        console.log(json)
        APIsubmitForm(json)
            .then(res => console.log(res));
    }

    render() {
        return (
            <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
                <Grid.Column style={{ maxWidth: 450 }}>
                    <Header as='h2' color='teal' textAlign='center'>
                        Log in to your account</Header>
                    <Form size='large' onSubmit={this.handleSubmit}>
                        <Segment stacked>
                            <Form.Input
                                fluid icon='user'
                                iconPosition='left'
                                placeholder='username'
                                onChange={this.handleChangeUser} />
                            <Form.Input
                                fluid
                                icon='lock'
                                iconPosition='left'
                                placeholder='Password'
                                type='password'
                                onChange={this.handleChangePassword}
                            />
                            <Button content='Login' primary />
                        </Segment>
                    </Form>
                </Grid.Column>
            </Grid>
        );
    }
}



export default LoginForm;