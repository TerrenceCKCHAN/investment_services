import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import LoginForm from './login/Login.js';
import Positions from './dashboards/positions/Positions.js';
import Position from './dashboards/positions/Position.js';
import DashMenu from './dashboards/DashMenu';
import Finances from './dashboards/Finances';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Router>
          <Route exact path="/" component={LoginForm} />
          <Route exact path="/positions" component={Positions} />
          <Route exact path="/position" component={Position} />
          <Route path="/dashmenu" component={DashMenu} />
          <Route path="/finances" component={Finances} />
        </Router>
      </header>
    </div>
  ); 
}


export default App;
